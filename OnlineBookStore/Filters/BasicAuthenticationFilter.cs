﻿using OnlineBookStore.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace OnlineBookStore.Filters
{
    public class BasicAuthenticationFilter : AuthorizationFilterAttribute
    {        
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Authorization.Equals(null))
            {
                actionContext.Response = actionContext.Request
                    .CreateErrorResponse(HttpStatusCode.Unauthorized,"Authorization Header Not Provided");
            }

            else
            {
                string authToken = actionContext.Request.Headers.Authorization.Parameter;
                string deCodedAuthToken = Encoding.UTF8.GetString(
                    Convert.FromBase64String(authToken));

                string[] usernamePasswordArray =
                    deCodedAuthToken.Split(':');
                string userId = usernamePasswordArray[0];
                string password = usernamePasswordArray[1];
                if (UserService.IsLoginCredentialValid(userId, password))
                {
                    var identity = new GenericIdentity(userId);
                    IPrincipal principal = new GenericPrincipal(identity, null);
                    Thread.CurrentPrincipal = principal;
                    if (HttpContext.Current != null)
                    {
                        HttpContext.Current.User = principal;
                    }

                }
                else
                {
                    actionContext.Response = actionContext.Request
                                        .CreateErrorResponse
                                        (HttpStatusCode.Unauthorized,"UnAuthorized");
                }

            }

        }
    }
}