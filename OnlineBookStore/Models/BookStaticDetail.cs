﻿using System.Collections.Generic;

namespace OnlineBookStore.Models
{
    public class BookStaticDetail
    {
        public static List<BookDetail> GetBookDetails()
        {
            List<BookDetail> bookDetailList = new List<BookDetail>();
            bookDetailList.Add(new BookDetail()
            {
                BookId = 435678,
                AuthorName = "Ashwin Sanghi",
                BookName = "The Vault of Vishnu",
                PublishedYear = "2020"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 435679,
                AuthorName = "Paul Coelho",
                BookName = "The Alchemist",                
                PublishedYear = "2006"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 435668,
                AuthorName = "Vivek Ranadive",
                BookName = "The Power to Predict",
                PublishedYear = "2006"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 435658,
                AuthorName = "Isaac Watts",
                BookName = "Divine Songs",
                PublishedYear = "1715"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 435378,
                AuthorName = "Sarah Fielding",
                BookName = "The Governess, or The Little Female Academy",
                PublishedYear = "1749"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 425678,
                AuthorName = "Christopher Smart",
                BookName = "The Parables of Our Lord and Saviour Jesus Christ",
                PublishedYear = "1768"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 432678,
                AuthorName = "Anna Laetitia Barbauld",
                BookName = "Lessons for Children",
                PublishedYear = "1779"

            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 415678,
                AuthorName = "Hannah More",
                BookName = "Sacred Dramas",
                PublishedYear = "1785"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 435178,
                AuthorName = "Andrew Grove",
                BookName = "A Memoir: Swimming Across",
                PublishedYear = "2001"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 435674,
                AuthorName = "Bill Cosby",
                BookName = "Fatherhood",
                PublishedYear = "1986"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 903421,
                AuthorName = "Damon Wayans",
                BookName = "Bootleg",
                PublishedYear = "1996"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 896542,
                AuthorName = "Chris Rock",
                BookName = "Rock This",
                PublishedYear = "1997"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 234681,
                AuthorName = "John Cleese",
                BookName = "So, Anyway...",
                PublishedYear = "2014"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 346786,
                AuthorName = "Andrew Dice Clay",
                BookName = "The Filthy Truth",
                PublishedYear = "2014"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 346788,
                AuthorName = "Fred Astaire",
                BookName = "I Like What I Know",
                PublishedYear = "1959"
            });
            bookDetailList.Add(new BookDetail()
            {
                BookId = 234568,
                AuthorName = "Ashwin Sanghi",
                BookName = "Steps in Time",
                PublishedYear = "1960"
            });

            return bookDetailList;

        }
    }
}