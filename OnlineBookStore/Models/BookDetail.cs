﻿namespace OnlineBookStore.Models
{
    public class BookDetail
    {
        public int BookId { get; set; }
        public string BookName { get; set; }
        public string AuthorName { get; set; }
        public string PublishedYear { get; set; }
    }
}