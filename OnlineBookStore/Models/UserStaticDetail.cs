﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineBookStore.Models
{
    public class UserStaticDetail
    {
        public List<User> GetUsers()
        {
            List<User> userList = new List<User>();
            userList.Add(new User()
            {
                UserId = "rama123@gmail.com",
                UserName = "Rinku",
                Password = "123456"
            });
            userList.Add(new User()
            {
                UserId = "manish123@yahoo.com",
                UserName = "Manish",
                Password = "abcdef"
            });
            return userList;
        }
    }
}