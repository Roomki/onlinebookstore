﻿using OnlineBookStore.Filters;
using OnlineBookStore.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OnlineBookStore.Controllers
{
    [BasicAuthenticationFilter]
    [Route("api/V1/Books")]
    public class BooksController : ApiController
    {

        public HttpResponseMessage GetBooks()
        {
            var res = BookStaticDetail.GetBookDetails();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
            return response;
        }    

        
    }
}
