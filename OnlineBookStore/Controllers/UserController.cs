﻿using OnlineBookStore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OnlineBookStore.Controllers
{
    
    [AllowAnonymous]
    [Route("api/V1/User")]
    public class UserController : ApiController
    {
        [HttpGet]        
        public HttpResponseMessage UserAuthenticate( string userId,  string password)
        {
           if(UserService.IsLoginCredentialValid(userId, password))
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { userId, password });
            }
           
            return  Request.CreateErrorResponse
                (HttpStatusCode.Unauthorized, "Username or password is incorrect");
        }
    }
}
